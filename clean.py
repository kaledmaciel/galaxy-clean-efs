# importing the required modules
import os
import shutil
import time
import datetime, time

# main function
def main():

	# initializing the count
	deleted_folders_count = 0
	deleted_files_count = 0

	# specify the path
	#path = "../../../../projetos/jotaja2/"
	#path = "../../../../efs/data-efs/files/boleto/"
	path = "../../../../efs/data-efs/logs/"

	# specify the days
	days = 90
	folders_pass = 0

	# converting days to seconds
	# time.time() returns current time in seconds
	agora = time.time()
	count = (days * 24 * 60 * 60)
	hoje = datetime.timedelta(seconds = agora)
	tempolimite = datetime.timedelta(seconds = count)


	# checking whether the file is present in path or not
	if os.path.exists(path):
		
		# iterating over each and every folder and file in the path
		print("Iniciando script\n")
		for root_folder, folders, files in os.walk(path):

			# comparing the days
			tempo = datetime.timedelta(seconds=get_file_or_folder_age(root_folder))
			contagem = hoje - tempo;
			print(root_folder + "\n")
			folders_pass = folders_pass + 1
			print(str(folders_pass))


			# checking the current directory files
			for file in files:

				#print(root_folder+"\n")
				# file path
				file_path = os.path.join(root_folder, file)

				# comparing the days


				gap = hoje.days - datetime.timedelta(seconds=get_file_or_folder_age(file_path)).days
				if gap >= tempolimite.days:
					print(file_path)
					print("Hoje:" + str(hoje))
					print(hoje.days - datetime.timedelta(seconds=get_file_or_folder_age(file_path)).days)
					print(datetime.timedelta(seconds=get_file_or_folder_age(file_path)))

					# invoking the remove_file function
					remove_file(file_path)
					deleted_files_count += 1 # incrementing count

		# else:

			# if the path is not a directory
			# comparing with the days
			# if seconds >= get_file_or_folder_age(path):
			#
			# 	# invoking the file
			# 	remove_file(path)
			# 	deleted_files_count += 1 # incrementing count

	else:

		# file/folder is not found
		print(f'"{path}" is not found')
		deleted_files_count += 1 # incrementing count

	#print(f"Total folders deleted: {deleted_folders_count}")
	print(f"Total files deleted: {deleted_files_count}")


def remove_folder(path):

	# removing the folder
	if not shutil.rmtree(path):

		# success message
		print(f"{path} is removed successfully")

	else:

		# failure message
		print(f"Unable to delete the {path}")



def remove_file(path):

	# removing the file
	if not os.remove(path):

		# success message
		print(f"{path} is removed successfully")

	else:

		# failure message
		print(f"Unable to delete the {path}")


def get_file_or_folder_age(path):

	# getting ctime of the file/folder
	# time will be in seconds
	ctime = os.stat(path).st_mtime

	# returning the time
	return ctime


if __name__ == '__main__':
	start_time = time.time()
	print(start_time)
	main()
	print("--- %s seconds ---" % (time.time() - start_time))
